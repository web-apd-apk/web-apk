@extends('layout.master')
@section('title')
    Halaman Akun Atasan
@endsection
@section('title-content')
    Tambah Data Atasan
@endsection
@section('content')
<form method="POST" action="/admin/akunatasan-add">
    @csrf
    <div class="form-group">
      <label for="namaatasan">Nama Atasan</label>
      <input type="" class="form-control" name="name" id="namaatasan" placeholder="Masukkan Nama Atasan">
    </div>
    <div class="form-group">
        <label for="emailatasan">Email Atasan</label>
        <input type="" class="form-control" name="email" value="@gmail.com" id="emailatasan" placeholder="Masukkan Email Atasan">
    </div>
    <div class="form-group">
        <label for="umuratasan">Umur</label>
        <input type="" class="form-control" name="umur" id="umuratasan" placeholder="Masukkan Umur">
    </div>
    <div class="form-group">
        <label for="alamatatasan">Alamat</label>
        <input type="" class="form-control" name="alamat" id="alamatatasan" placeholder="Masukkan alamat">
    </div>
    <div class="form-group" style="display: ">
        <label for="passwordatasan">Password</label>
        <input type="" class="form-control" name="password" value="12345678" id="passwordatasan" placeholder="Masukkan nama kelas">
    </div>
    <div class="form-group">
        <label for="role">Pilih Role</label>
        <select class="form-control" name="role" id="role">
            <option value="manajer">Manajer</option>
            <option value="supervisor">Supervisor</option>
            <option value="tim_k3">Tim-K3</option>
            <option value="admin">Admin</option>
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection