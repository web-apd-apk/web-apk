@extends('layout.master')
@section('title')
    Halaman Akun Atasan
@endsection
@section('title-content')
    Data Akun Atasan
@endsection
@section('content')
<a href="/admin/akunatasan/add-dataatasan" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Akun</a>
<div class="card-body">
    <div class="table-responsive">
        <table id="dataTable" class="table table-bordered table-hover" >
            <thead>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Umur</th>
                    <th>Alamat</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Umur</th>
                    <th>Alamat</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody align="center">
                @foreach ($atasan as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->user->email}}</td>
                        <td>{{$item->umur}}</td>
                        <td>{{$item->alamat}}</td>
                        <td>{{$item->user->role}}</td>
                        <td><a type="button" href="/admin/akunatasan/delete{{ $item->id }}" class="btn btn-danger m-1">Hapus</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection