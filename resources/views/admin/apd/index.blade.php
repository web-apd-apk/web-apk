@extends('layout.master')
@section('title')
    Halaman APD
@endsection
@section('title-content')
    Daftar APD
@endsection
@section('content')
<a href="/admin/apd/add" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Apd</a>
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Nama</th>
                    <th>Jenis</th>
                    <th>Stock Existing</th>
                    <th>Safety Stock</th>
                    <th>ROP Stock</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr align="center">
                    <th>No</th>
                    <th>Nama</th>
                    <th>Jenis</th>
                    <th>Stock Existing</th>
                    <th>Safety Stock</th>
                    <th>ROP Stock</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody align="center">
                @foreach ($apd as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->name}}</td>
                        @if ($item->type)
                            <td>APD</td>
                        @else
                            <td>APK</td>
                        @endif
                        <td>{{$item->stock}}</td>
                        <td>{{$item->safety_stock}}</td>
                        <td>{{$item->rop_stock}}</td>
                        <td>
                            {{-- <a type="button" href="/admin/databagian/delete{{$item->id}}" class="btn btn-warning m-1">Ubah Stok</a> --}}
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal">
                                Ubah Stok
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                <form method="POST" action="/admin/apd/updatestok">
                                @csrf
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Update stok {{$item->name}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <input type="text" name="id" value="{{$item->id}}" style="display: none">
                                            <div class="form-group">
                                                <label for="stock">Stock</label>
                                                <input type="number" class="form-control" name="stock" id="stock" placeholder="Masukkan jumlah safety stock">
                                            </div>
                                            <div class="form-group">
                                                <label for="namabagian">Stock Safety</label>
                                                <input type="number" class="form-control" name="safety_stock" id="namabagian" placeholder="Masukkan jumlah safety stock">
                                            </div>
                                            <div class="form-group">
                                                <label for="namabagian">ROP Safety</label>
                                                <input type="number" class="form-control" name="rop_stock" id="namabagian" placeholder="Masukkan jumlah ROP stock">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection