@extends('layout.master')
@section('title')
    Halaman APD
@endsection
@section('title-content')
    Tambah APD
@endsection
@section('content')
<form method="POST" action="/admin/apd/add">
    @csrf
    <div class="form-group">
        <label for="namabagian">Nama APD</label>
        <input type="" class="form-control" name="name" id="namabagian" placeholder="Masukkan Nama APD">
    </div>
    <div class="form-group">
        <label for="bagian">Pilih Jenis</label>
        <select class="form-control" name="type" id="bagian">
            <option value="1">APD</option>
            <option value="2">APK</option>
        </select>
    </div>
    <div class="form-group">
        <label for="stock">Stock Gudang</label>
        <input type="number" class="form-control" name="stock" id="stock" placeholder="Masukkan jumlah stock gudang">
    </div>
    <div class="form-group">
        <label for="namabagian">Stock Safety</label>
        <input type="number" class="form-control" name="safety_stock" id="namabagian" placeholder="Masukkan jumlah safety stock">
    </div>
    <div class="form-group">
        <label for="namabagian">ROP Safety</label>
        <input type="number" class="form-control" name="rop_stock" id="namabagian" placeholder="Masukkan jumlah ROP stock">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection