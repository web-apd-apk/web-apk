@extends('layout.master')
@section('title')
    Halaman Pengajuan
@endsection
@section('title-content')
    Pengajuan APD
@endsection
@section('content')
<form method="POST" action="/admin/pengajuanapd/add" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="pelpaor">Nama pengaju</label>
        <input type="" class="form-control" name="pelapor" value="{{$user->name}}" id="pelapor" disabled>
    </div>
    <div class="form-group">
        <label for="bagian">Pilih Bagian</label>
        <select class="form-control" name="bagian" id="bagian">
            @foreach ($bagian as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="type">Pilih Jenis</label>
        <select class="form-control" name="type" id="type">
            <option selected disabled>-- Pilih Jenis APD --</option>
            <option value="1">APD</option>
            <option value="2">APK</option>
        </select>
    </div>
    <div class="form-group">
        <label for="apd">Pilih APD / APK</label>
        <select class="form-control" name="apd" id="apd">
            <option selected disabled>-- Pilih jenis APD dahulu--</option>
        </select>
    </div>
    <div class="form-group">
        <label for="jumlah">Jumlah</label>
        <input type="" class="form-control" name="jumlah" value="" id="jumlah">
    </div>
    <div class="form-group">
        <label for="bukti">Bukti</label>
        <input type="file" class="form-control" name="bukti" value="" id="bukti">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('select').select();
    });

    $('#type').on('change', function(e) {
        // console.log(e);
        var type = e.target.value;
        // alert(e.target.value);
        $.get('/getdata/apd/'+ type,
            function(data) {
                console.log(data);
                $('#apd').empty();

                $('#apd').append($("<option>")
                    .text("-- Pilih APD --")
                );
                $.each(data, function(index, apd) {
                    $('#apd').append($("<option>")
                        .attr("value", apd.id)
                        .text(apd.name)
                    );
                })

                $('select').select();
            });
    });
</script>
@endsection