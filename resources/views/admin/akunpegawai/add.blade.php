@extends('layout.master')
@section('title')
    Halaman Akun Pegawai
@endsection
@section('title-content')
    Tambah Data Pegawai
@endsection
@section('content')
<form method="POST" action="/admin/akunpegawai-add">
    @csrf
    <div class="form-group">
      <label for="namapegawai">Nama Pegawai</label>
      <input type="" class="form-control" name="name" id="namapegawai" placeholder="Masukkan Nama Pegawai">
    </div>
    <div class="form-group">
        <label for="emailpegawai">Email Pegawai</label>
        <input type="" class="form-control" name="email" value="@gmail.com" id="emailpegawai" placeholder="Masukkan Email Pegawai">
    </div>
    <div class="form-group">
        <label for="umurpegawai">Umur</label>
        <input type="" class="form-control" name="umur" id="umurpegawai" placeholder="Masukkan Umur">
    </div>
    <div class="form-group">
        <label for="alamatpegawai">Alamat</label>
        <input type="" class="form-control" name="alamat" id="alamatpegawai" placeholder="Masukkan alamat">
    </div>
    <div class="form-group" style="display: ">
        <label for="passwordpegawai">Password</label>
        <input type="" class="form-control" name="password" value="12345678" id="passwordpegawai" placeholder="Masukkan nama kelas">
    </div>
    <div class="form-group">
        <label for="bagian">Pilih Bagian</label>
        <select class="form-control" name="id_bagian" id="bagian">
            @foreach ($bagian as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection