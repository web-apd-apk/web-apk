@extends('layout.master')
@section('title')
    Halaman Akun Pegawai
@endsection
@section('title-content')
    Data Akun Pegawai
@endsection
@section('content')
<a href="/admin/akunpegawai/add-datapegawai" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Akun</a>
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Umur</th>
                    <th>Alamat</th>
                    <th>Bagian</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Umur</th>
                    <th>Alamat</th>
                    <th>Bagian</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody align="center">
                @foreach ($pegawai as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->user->email}}</td>
                        <td>{{$item->umur}}</td>
                        <td>{{$item->alamat}}</td>
                        <td>{{$item->bagian->name}}</td>
                        <td><a type="button" href="/admin/akunpegawai/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection