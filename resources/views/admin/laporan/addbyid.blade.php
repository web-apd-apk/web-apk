@extends('layout.master')
@section('title')
    Halaman Laporan
@endsection
@section('title-content')
    Laporkan inspeksi {{$inspeksi->user->name}} - {{$inspeksi->bagian->name}}
@endsection
@section('content')
<form method="POST" action="/admin/laporankerusakan/add/{{$inspeksi->id}}">
    @csrf
    <div class="form-group">
        <label for="timinspeksi">Tim Inspeksi</label>
        <input type="" class="form-control" name="timinspeksi" value="{{$inspeksi->user->name}}" id="timinspeksi" disabled>
    </div>
    <div class="form-group">
        <label for="lokasi">Lokasi</label>
        <input type="" class="form-control" name="lokasi" value="{{$inspeksi->lokasi}}" id="lokasi" disabled>
    </div>
    <div class="form-group">
        <label for="bagian">Bagian</label>
        <input type="" class="form-control" name="bagian" value="{{$inspeksi->bagian->name}}" id="bagian" disabled>
    </div>
    <div class="form-group">
        <label for="apd">APD</label>
        <input type="" class="form-control" name="apd" value="{{$inspeksi->apd->name}}" id="apd" disabled>
    </div>
    <div class="form-group">
        <label for="jumlah">Jumlah</label>
        <input type="" class="form-control" name="jumlah" value="{{$inspeksi->jumlah}}" id="jumlah" disabled>
    </div>
    <div class="form-group">
        <label for="keterangan">Keterangan</label>
        <input type="" class="form-control" name="keterangan" value="" id="keterangan">
    </div>
    <div class="form-group">
        <label for="tindak_lanjut">Tindak Lanjut</label>
        <input type="" class="form-control" name="tindak_lanjut" value="" id="tindak_lanjut">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection