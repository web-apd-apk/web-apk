@extends('layout.master')
@section('title')
    Halaman Laporan APD
@endsection
@section('title-content')
    Daftar Hasil Laporan APD
@endsection
@section('content')
<a href="/admin/laporankerusakan/add" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Laporan</a>
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Nama Pelapor</th>
                    <th>Lokasi</th>
                    <th>Bagian</th>
                    <th>APD</th>
                    <th>Jumlah</th>
                    <th>Inspeksi</th>
                    <th>Tindak lanjut</th>
                    <th>Keterangan</th>
                </tr>
            </thead>
            <tfoot>
                <tr align="center">
                    <th>No</th>
                    <th>Nama Pelapor</th>
                    <th>Lokasi</th>
                    <th>Bagian</th>
                    <th>APD</th>
                    <th>Jumlah</th>
                    <th>Inspeksi</th>
                    <th>Tindak lanjut</th>
                    <th>Keterangan</th>
                </tr>
            </tfoot>
            <tbody align="center">
                @foreach ($laporan as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->user->name}}</td>
                        <td>{{$item->inspeksi->lokasi}}</td>
                        <td>{{$item->inspeksi->bagian->name}}</td>
                        <td>{{$item->inspeksi->apd->name}}</td>
                        <td>{{$item->inspeksi->jumlah}}</td>
                        <td>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                Detail
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Detail Inspeksi</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Tim Inspeksi : {{$item->inspeksi->user->name}}</p>
                                        <p>Lokasi : {{$item->inspeksi->lokasi}}</p>
                                        <p>Bagian : {{$item->inspeksi->bagian->name}}</p>
                                        <p>APD : {{$item->inspeksi->apd->name}}</p>
                                        <p>Jumlah : {{$item->inspeksi->jumlah}}</p>
                                        <p>Tindak Lanjut : {{$item->inspeksi->tindak_lanjut}}</p>
                                        <p>Keterangan : {{$item->inspeksi->keterangan}}</p>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </td>
                        <td>{{$item->tindak_lanjut}}</td>
                        <td>{{$item->keterangan}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection 