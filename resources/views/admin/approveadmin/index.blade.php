@extends('layout.master')
@section('title')
    Halaman Pengajuan APD
@endsection
@section('title-content')
    Daftar Pengajuan APD
@endsection
@section('content')
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Nama Pengaju</th>
                    <th>Bagian</th>
                    <th>APD</th>
                    <th>Jumlah</th>
                    <th>Bukti</th>
                    <th>Approval SPV K3</th>
                    <th>Approval Manajer K3</th>
                    <th>Approval Admin</th>
                </tr>
            </thead>
            <tfoot>
                <tr align="center">
                    <th>No</th>
                    <th>Nama Pengaju</th>
                    <th>Bagian</th>
                    <th>APD</th>
                    <th>Jumlah</th>
                    <th>Bukti</th>
                    <th>Approval SPV K3</th>
                    <th>Approval Manajer K3</th>
                    <th>Approval Admin</th>
                </tr>
            </tfoot>
            <tbody align="center">
                @foreach ($pengajuan as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->user->name}}</td>
                        <td>{{$item->bagian->name}}</td>
                        <td>{{$item->apd->name}}</td>
                        <td>{{$item->jumlah}}</td>
                        <td><a href="/download/buktipengajuan/{{$item->id}}" class="d-none d-sm-inline-block btn btn-sm btn-warning shadow-sm mb-3">Download</a></td>
                        @if ($item->apv_spv_k3 == null)
                            <td>Belum Approve</td>
                        @elseif($item->apv_spv_k3 == -1)
                            <td style="color: red">Ditolak Supervisor</td>
                        @else
                            <td>{{$item->spvk3->name}}</td>
                        @endif
                        @if ($item->apv_mnj_k3 == null)
                            <td>Belum Approve</td>
                        @elseif($item->apv_mnj_k3 == -1)
                            <td style="color: red">Ditolak Manajer</td>
                        @else
                            <td>{{$item->mnjk3->name}}</td>
                        @endif
                        @if ($item->apv_mnj_k3 == null)
                            <td>Belum Approve Manajer</td>
                        @elseif($item->apv_mnj_k3 == -1)
                            <td>Ditolak Manajer</td>
                        @else
                            @if ($item->apv_admin == null)
                            <td>
                                <a href="/admin/approvepengajuan/approve/{{$item->id}}" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm mb-3">Approve</a>
                                <a href="/admin/approvepengajuan/decline/{{$item->id}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm mb-3">Tolak</a>
                            </td>
                            @elseif($item->apv_admin == -1)
                                <td style="color: red">Ditolak Admin</td>
                            @else
                                <td>{{$item->admin->name}}</td>
                            @endif
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection 