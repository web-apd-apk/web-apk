@extends('layout.master')
@section('title')
    Halaman Pengajuan Pemusnahan
@endsection
@section('title-content')
    Daftar Pengajuan Pemusnahan
@endsection
@section('content')
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Nama Pemusnah</th>
                    <th>Nama Pemilik</th>
                    <th>Jenis</th>
                    <th>APD/APK</th>
                    <th>Jumlah</th>
                    <th>Approval SPV K3</th>
                    <th>Approval Manajer K3</th>
                    <th>Approval Admin</th>
                </tr>
            </thead>
            <tfoot>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Nama Pemusnah</th>
                    <th>Nama Pemilik</th>
                    <th>Jenis</th>
                    <th>APD/APK</th>
                    <th>Jumlah</th>
                    <th>Approval SPV K3</th>
                    <th>Approval Manajer K3</th>
                    <th>Approval Admin</th>
                </tr>
            </tfoot>
            <tbody align="center">
                @foreach ($pemusnahan as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->pemusnah}}</td>
                            <td>{{$item->pemilik}}</td>
                            @if ($item->apd->type == 1)
                            <td>APD</td>
                            @elseif ($item->apd->type == 2)
                            <td>APK</td>
                            @endif
                            <td>{{$item->apd->name}}</td>
                            <td>{{$item->jumlah}}</td>
                            @if ($item->apv_spv_k3 == null)
                            <td>Belum Approve</td>
                            @elseif($item->apv_spv_k3 == -1)
                                <td style="color: red">Ditolak Supervisor</td>
                            @else
                                <td>{{$item->spvk3->name}}</td>
                            @endif
                            @if ($item->apv_mnj_k3 == null)
                                <td>Belum Approve</td>
                            @elseif($item->apv_mnj_k3 == -1)
                                <td style="color: red">Ditolak Manajer</td>
                            @else
                                <td>{{$item->mnjk3->name}}</td>
                            @endif
                            @if ($item->apv_mnj_k3 == null)
                                <td>Belum Approve Manajer</td>
                            @elseif($item->apv_mnj_k3 == -1)
                                <td>Ditolak Manajer</td>
                            @else
                                @if ($item->apv_admin == null)
                                <td>
                                    <a href="/admin/approvepemusnahan/approve/{{$item->id}}" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm mb-3">Approve</a>
                                    <a href="/admin/approvepemusnahan/decline/{{$item->id}}" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm mb-3">Tolak</a>
                                </td>
                                @elseif($item->apv_admin == -1)
                                    <td style="color: red">Ditolak Admin</td>
                                @else
                                    <td>{{$item->admin->name}}</td>
                                @endif
                            @endif
                        </tr>
                    @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection 