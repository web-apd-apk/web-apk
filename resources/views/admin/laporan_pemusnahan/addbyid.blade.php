@extends('layout.master')
@section('title')
    Halaman Laporan
@endsection
@section('title-content')
    Laporkan Pemusnahan
@endsection
@section('content')
<form method="POST" action="/admin/laporanpemusnahan/add/{{$pemusnahan->id}}" enctype="multipart/form-data">
    @csrf
    <input type="text" value="{{$pemusnahan->id}}" name="id" style="display: none">
    <div class="form-group">
        <label for="pemusnah">Nama Pemusnah</label>
        <input type="" class="form-control" name="pemusnah" value="{{$pemusnahan->pemusnah}}" id="pemusnah" disabled>
    </div>
    <div class="form-group">
        <label for="pemilik">Nama Pemilik</label>
        <input type="" class="form-control" name="pemilik" value="{{$pemusnahan->pemilik}}" id="pemilik" disabled>
    </div>
    @if ($pemusnahan->apd->type==1)
        @php
            $value='APD'
        @endphp
    @else
        @php
            $value='APK'
        @endphp
    @endif
    <div class="form-group">
        <label for="jenis">jenis</label>
        <input type="" class="form-control" name="jenis" value="{{$value}}" id="jenis" disabled>
    </div>
    <div class="form-group">
        <label for="apd">APD/APK</label>
        <input type="" class="form-control" name="apd" value="{{$pemusnahan->apd->name}}" id="apd" disabled>
    </div>
    <div class="form-group">
        <label for="jumlah">jumlah</label>
        <input type="" class="form-control" name="jumlah" value="{{$pemusnahan->jumlah}}" id="jumlah" disabled>
    </div>
    <div class="form-group">
        <label for="exampleFormControlFile1">Bukti</label>
        <input type="file" name="bukti" class="form-control-file" id="exampleFormControlFile1">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection