@extends('layout.master')
@section('title')
    Halaman Laporan Pemusnahan
@endsection
@section('title-content')
    Data Pemusnahan
@endsection
@section('content')
<a href="" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Laporan</a>
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Nama Pelapor</th>
                    <th>Nama Pemilik</th>
                    <th>Jenis</th>
                    <th>APD/APK</th>
                    <th>Jumlah</th>
                    <th width="200px">Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr align="center">
                    <th width="200px">No</th>
                    <th>Nama Pelapor</th>
                    <th>Nama Pemilik</th>
                    <th>Jenis</th>
                    <th>APD/APK</th>
                    <th>Jumlah</th>
                    <th width="100px">Action</th>
                </tr>
            </tfoot>
            <tbody align="center">
                @foreach ($pemusnahan as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->pemusnah}}</td>
                        <td>{{$item->pemilik}}</td>
                        @if ($item->apd->type == 1)
                        <td>APD</td>
                        @elseif ($item->apd->type == 2)
                        <td>APK</td>
                        @endif
                        <td>{{$item->apd->name}}</td>
                        <td>{{$item->jumlah}}</td>
                        
                        <td>
                            <a href="/admin/laporanpemusnahan/add/{{ $item->id }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Pilih</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection 