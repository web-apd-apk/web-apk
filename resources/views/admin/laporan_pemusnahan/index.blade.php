@extends('layout.master')
@section('title')
    Halaman Laporan Pemusnahan
@endsection
@section('title-content')
    Daftar Hasil Laporan Pemusnahan
@endsection
@section('content')
<a href="/admin/laporanpemusnahan/add" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Laporan</a>
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Nama Pelapor</th>
                    <th>Nama Pemilik</th>
                    <th>Jenis</th>
                    <th>APD/APK</th>
                    <th>Jumlah</th>
                    <th>Bukti</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Nama Pelapor</th>
                    <th>Nama Pemilik</th>
                    <th>Jenis</th>
                    <th>APD/APK</th>
                    <th>Jumlah</th>
                    <th>Bukti</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody align="center">
                @foreach ($laporanPemusnahan as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->pemusnahan->pemusnah}}</td>
                        <td>{{$item->pemusnahan->pemilik}}</td>
                        @if ($item->pemusnahan->apd->type == 1)
                        <td>APD</td>
                        @elseif ($item->pemusnahan->apd->type == 2)
                        <td>APK</td>
                        @endif
                        <td>{{$item->pemusnahan->apd->name}}</td>
                        <td>{{$item->pemusnahan->jumlah}}</td>
                        <td><a href="/download/buktipemusnahan/{{$item->id}}" class="d-none d-sm-inline-block btn btn-sm btn-warning shadow-sm mb-3">Download</a></td>
                        <td><a type="button" href="" class="btn btn-danger m-1">Cetak</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection 