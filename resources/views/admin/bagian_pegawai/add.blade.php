@extends('layout.master')
@section('title')
    Halaman Bagian Pegawai
@endsection
@section('title-content')
    Tambah Bagian Pegawai
@endsection
@section('content')
<form method="POST" action="/admin/add-databagian">
    @csrf
    <div class="form-group">
        <label for="namabagian">Nama Bagian</label>
        <input type="" class="form-control" name="name" id="namabagian" placeholder="Masukkan Nama Bagian Pegawai">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection