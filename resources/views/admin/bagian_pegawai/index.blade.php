@extends('layout.master')
@section('title')
    Halaman Bagian Pegawai
@endsection
@section('title-content')
    Daftar Bagian Pegawai
@endsection
@section('content')
<a href="/admin/add/databagian" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Bagian</a>
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Jenis Bagian</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr align="center">
                    <th>No</th>
                    <th>Jenis Bagian</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody align="center">
                @foreach ($bagian as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->name}}</td>
                        <td><a type="button" href="/admin/databagian/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection