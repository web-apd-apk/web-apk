@extends('layout.master')
@section('title')
    Halaman Inspeksi APD
@endsection
@section('title-content')
    Tambah Inspeksi APD
@endsection
@section('content')
<form method="POST" action="/admin/inspeksiapd/add">
    @csrf
    <div class="form-group">
        <label for="lokasi">Lokasi Temuan</label>
        <input type="" class="form-control" name="lokasi" id="lokasi" placeholder="Masukkan Lokasi Temuan">
    </div>
    <div class="form-group">
        <label for="bagian">Pilih Bagian</label>
        <select class="form-control" name="bagian" id="bagian">
            @foreach ($bagian as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="type">Pilih Jenis</label>
        <select class="form-control" name="type" id="type">
            <option selected disabled>-- Pilih Jenis APD --</option>
            <option value="1">APD</option>
            <option value="2">APK</option>
        </select>
    </div>
    <div class="form-group">
        <label for="apd">Pilih APD / APK</label>
        <select class="form-control" name="apd" id="apd">
            <option selected disabled>-- Pilih jenis APD dahulu--</option>
        </select>
    </div>
    <div class="form-group">
        <label for="jumlah">Jumlah</label>
        <input type="number" class="form-control" name="jumlah" id="jumlah" placeholder="Masukkan jumlah">
    </div>
    <div class="form-group">
        <label for="kondisi">Kondisi</label>
        <select class="form-control" name="kondisi" id="kondisi">
            <option selected disabled>-- Pilih Kondisi --</option>
            <option value="1">Layak</option>
            <option value="2">Tidak Layak</option>
        </select>
    </div>
    <div id="place">
    </div>
    <div class="form-group">
        <label for="keterangan">Keterangan</label>
        <input type="" class="form-control" name="keterangan" id="keterangan" placeholder="Masukkan Keterangan Inspeksi">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('select').select();
    });

    $('#type').on('change', function(e) {
        // console.log(e);
        var type = e.target.value;
        // alert(e.target.value);
        $.get('/getdata/apd/'+ type,
            function(data) {
                console.log(data);
                $('#apd').empty();

                $('#apd').append($("<option>")
                    .text("-- Pilih APD --")
                );
                $.each(data, function(index, apd) {
                    $('#apd').append($("<option>")
                        .attr("value", apd.id)
                        .text(apd.name)
                    );
                })

                $('select').select();
            });
    });

    $('#kondisi').on('change', function(e) {
        // console.log(e);
        var kondisi = e.target.value;
        let html = '';
        // alert(e.target.value);
        if (kondisi == 2) {
            html +='<div class="form-group"> <label for="tindak_lanjut">Tindak Lanjut</label> <input type="" class="form-control" name="tindak_lanjut" id="tindak_lanjut" placeholder="Masukkan Tindak Lanjut"> </div>'

            $('#place').append(html)
        }else{
            $('#place').empty();
        }
        // $.get('/getdata/apd/'+ type,
        //     function(data) {
        //         console.log(data);
        //         $('#apd').empty();

        //         $('#apd').append($("<option>")
        //             .text("-- Pilih APD --")
        //         );
        //         $.each(data, function(index, apd) {
        //             $('#apd').append($("<option>")
        //                 .attr("value", apd.id)
        //                 .text(apd.name)
        //             );
        //         })

        //         $('select').select();
        //     });
    });
</script>
@endsection