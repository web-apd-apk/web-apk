@extends('layout.master')
@section('title')
    Halaman Inspeksi APD
@endsection
@section('title-content')
    Daftar Hasil Inspeksi APD
@endsection
@section('content')
<a href="/admin/inspeksiapd/add" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Hasil Inspeksi APD</a>
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Tim inspeksi</th>
                    <th>Lokasi</th>
                    <th>Bagian</th>
                    <th>APD</th>
                    <th>Jumlah</th>
                    <th>Kondisi</th>
                    <th>Tindak lanjut</th>
                    <th>Keterangan</th>
                </tr>
            </thead>
            <tfoot>
                <tr align="center">
                    <th>No</th>
                    <th>Nama</th>
                    <th>Lokasi</th>
                    <th>Bagian</th>
                    <th>APD</th>
                    <th>Jumlah</th>
                    <th>Kondisi</th>
                    <th>Tindak lanjut</th>
                    <th>Keterangan</th>
                </tr>
            </tfoot>
            <tbody align="center">
                @foreach ($inspeksi as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->user->name}}</td>
                        <td>{{$item->lokasi}}</td>
                        <td>{{$item->bagian->name}}</td>
                        <td>{{$item->apd->name}}</td>
                        <td>{{$item->jumlah}}</td>
                        @if ($item->kondisi == 1)
                            <td>Layak</td>
                        @else
                            <td>Tidak Layak</td>
                        @endif
                        <td>{{$item->tindak_lanjut}}</td>
                        <td>{{$item->keterangan}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection 