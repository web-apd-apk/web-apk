<?php

namespace Database\Seeders;

use App\Models\Bagian;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BagianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $bagian = new Bagian;
        $bagian->name = 'EPB';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'TSI';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'KAP';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'TU & HUMAS';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'HUKUM';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'LI';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'RENSTRA';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'KINERJA & STANDAR';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'QC';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PKA';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'POL';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PKU';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PPK';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PROG NG';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PROG KP';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'STDU';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PEMELIHARAAN PROD';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PPP';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PPE';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'LAB UJI AIR';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PPD';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PPO';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PTT';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'SDT';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PTB';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'SDB';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PEMAKAIAN AIR';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PENERTIBAN';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'HUBLANG';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'ANGGARAN & KAS';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'AKN';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'REK';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PENGADAAN';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'LOG';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'SPSDM';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'PK3';
        $bagian->save();
        $bagian = new Bagian;
        $bagian->name = 'KPNT';
        $bagian->save();        
    }
}
