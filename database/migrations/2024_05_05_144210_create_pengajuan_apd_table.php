<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pengajuan_apd', function (Blueprint $table) {
            $table->id();
            $table->integer('id_user');
            $table->integer('id_bagian');
            $table->integer('id_apd');
            $table->integer('jumlah');
            $table->string('bukti');
            $table->integer('apv_admin')->nullable();
            $table->integer('apv_spv_k3')->nullable();
            $table->integer('apv_mnj_k3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pengajuan_apd');
    }
};
