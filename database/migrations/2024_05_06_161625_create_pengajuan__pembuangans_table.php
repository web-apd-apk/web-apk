<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pengajuan__pembuangan', function (Blueprint $table) {
            $table->id();
            $table->string('pemusnah');
            $table->string('pemilik');
            $table->integer('id_apd');
            $table->integer('jumlah');
            $table->integer('apv_admin');
            $table->integer('apv_spv_k3');
            $table->integer('apv_mnj_k3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pengajuan__pembuangan');
    }
};
