<?php

namespace App\Http\Controllers;

use App\Models\Inspeksi;
use App\Models\Laporan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LaporanController extends Controller
{
    public function index()
    {
        $laporan = Laporan::get();
        return view('admin.laporan.index', compact('laporan'));
    }

    public function indexadd()
    {
        $inspeksi = Inspeksi::where('kondisi',2)->where('status',0)->get();
        return view('admin.laporan.add', compact('inspeksi'));
    }

    public function indexaddbyid($id)
    {
        $inspeksi = Inspeksi::where('id',$id)->first();
        return view('admin.laporan.addbyid', compact('inspeksi'));
    }

    public function actionaddbyid(Request $request,$id){
        $laporan = new Laporan;
        $laporan->id_user = Auth::user()->id;
        $laporan->id_inspeksi = $id;
        $laporan->keterangan = $request->keterangan;
        $laporan->tindak_lanjut = $request->tindak_lanjut;
        $laporan->save();

        $inspeksi = Inspeksi::where('id',$id)->first();
        $inspeksi->status = 1;
        $inspeksi->save();

        return redirect('/admin/laporankerusakan');
    }
}
