<?php

namespace App\Http\Controllers;

use App\Models\Apd;
use Illuminate\Http\Request;

class ApdController extends Controller
{
    //
    public function index()
    {
        $apd = Apd::get();
        return view('admin.apd.index', compact('apd'));
    }

    public function indexadd()
    {
        return view('admin.apd.add');
    }

    public function actionadd(Request $request)
    {
        // dd($request->input());
        $apd = new Apd;
        $apd->name = $request->name;
        $apd->type = $request->type;
        $apd->stock = $request->stock;
        $apd->safety_stock = $request->safety_stock;
        $apd->rop_stock = $request->rop_stock;
        $apd->save();
        return redirect('/admin/apd');
    }

    public function updatestok(Request $request)
    {
        $apd = Apd::where('id',$request->id)->first();
        $apd->stock = $request->stock;
        $apd->safety_stock = $request->safety_stock;
        $apd->rop_stock = $request->rop_stock;
        $apd->save();
        return redirect('/admin/apd');
    }

    public function getdataapd(Request $request,$id){
        $apd = APD::where('type', $id)->get();
        if ($request->ajax()) {
            return response()->json($apd);
        }
    }
}
