<?php

namespace App\Http\Controllers;

use App\Models\Bagian;
use App\Models\Pengajuan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PengajuanController extends Controller
{
    public function index()
    {
        $pengajuan = Pengajuan::where('id_user',Auth::user()->id)->get();
        return view('admin.pengajuan.index', compact('pengajuan'));
    }

    public function indexadd()
    {
        $user = User::where('id',Auth::user()->id)->first();
        $bagian = Bagian::get();
        return view('admin.pengajuan.add', compact('bagian', 'user'));
    }

    public function actionadd(Request $request)
    {
        $user = User::where('id',Auth::user()->id)->first();
        $namafile = time() . '.' . $request->bukti->extension();
        $request->bukti->move(storage_path('buktipengajuan'), $namafile);

        $pengajuan = new Pengajuan;
        $pengajuan->id_user = $user->id;
        $pengajuan->id_bagian = $request->bagian;
        $pengajuan->id_apd = $request->apd;
        $pengajuan->jumlah = $request->jumlah;
        $pengajuan->bukti = $namafile;
        $pengajuan->save();
        
        return redirect('/admin/pengajuanapd');
    }

    public function downloadbukti($id)
    {
        $pengajuan = Pengajuan::where('id',$id)->first();
        
        return response()->download(storage_path('buktipengajuan/'.$pengajuan->bukti));
    }

    public function approveadmin()
    {
        $pengajuan = Pengajuan::get();
        return view('admin.approveadmin.index', compact('pengajuan'));
    }

    public function approvedbyadmin($id)
    {
        $pengajuan = Pengajuan::where('id',$id)->first();
        $pengajuan->apv_admin = Auth::user()->id;
        $pengajuan->save();

        return redirect('/admin/approvepengajuan');
    }

    public function declinedbyadmin($id)
    {
        $pengajuan = Pengajuan::where('id',$id)->first();
        $pengajuan->apv_admin = -1;
        $pengajuan->save();

        return redirect('/admin/approvepengajuan');
    }

    public function approvemanajer()
    {
        $pengajuan = Pengajuan::get();
        return view('manajer.approvemanajer.index', compact('pengajuan'));
    }

    public function approvedbymanajer($id)
    {
        $pengajuan = Pengajuan::where('id',$id)->first();
        $pengajuan->apv_mnj_k3 = Auth::user()->id;
        $pengajuan->save();

        return redirect('/manajer/approvepengajuan');
    }

    public function declinedbymanajer($id)
    {
        $pengajuan = Pengajuan::where('id',$id)->first();
        $pengajuan->apv_mnj_k3 = -1;
        $pengajuan->save();

        return redirect('/manajer/approvepengajuan');
    }

    public function approvesupervisor()
    {
        $pengajuan = Pengajuan::get();
        return view('supervisor.approvesupervisor.index', compact('pengajuan'));
    }

    public function approvedbysupervisor($id)
    {
        $pengajuan = Pengajuan::where('id',$id)->first();
        $pengajuan->apv_spv_k3 = Auth::user()->id;
        $pengajuan->save();

        return redirect('/supervisor/approvepengajuan');
    }

    public function declinedbysupervisor($id)
    {
        $pengajuan = Pengajuan::where('id',$id)->first();
        $pengajuan->apv_spv_k3 = -1;
        $pengajuan->save();

        return redirect('/supervisor/approvepengajuan');
    }
}
