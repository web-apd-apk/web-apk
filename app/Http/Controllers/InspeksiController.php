<?php

namespace App\Http\Controllers;

use App\Models\Bagian;
use App\Models\Inspeksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InspeksiController extends Controller
{
    public function index()
    {
        $inspeksi = Inspeksi::get();
        return view('admin.inspeksi.index', compact('inspeksi'));
    }

    public function indexadd()
    {
        $bagian = Bagian::get();
        return view('admin.inspeksi.add', compact('bagian'));
    }

    public function actionadd(Request $request)
    {
        // dd($request->input());
        $inspeksi = new Inspeksi;
        $inspeksi->id_user = Auth::user()->id;
        $inspeksi->lokasi = $request->lokasi;
        $inspeksi->id_bagian = $request->bagian;
        $inspeksi->id_apd = $request->apd;
        $inspeksi->jumlah = $request->jumlah;
        $inspeksi->kondisi = $request->kondisi;
        if ($request->tindak_lanjut) {
            $inspeksi->tindak_lanjut = $request->tindak_lanjut;
        }
        $inspeksi->keterangan = $request->keterangan;
        $inspeksi->save();

        return redirect('/admin/inspeksiapd');
    }
}
