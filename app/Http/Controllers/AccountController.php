<?php

namespace App\Http\Controllers;

use App\Models\Atasan;
use App\Models\Bagian;
use App\Models\Pegawai;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    //Akun Pegawai
    public function indexakunpegawai()
    {
        $bagian = Bagian::get();
        $pegawai = Pegawai::get();
        // dd($pegawai);
        return view('admin.akunpegawai.index', compact('bagian', 'pegawai'));
    }

    public function indexaddakunpegawai()
    {
        $bagian = Bagian::get();
        return view('admin.akunpegawai.add', compact('bagian'));
    }

    public function addakunpegawai(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = 'user_bagian';
        $user->password = Hash::make($request->password);
        $user->save();

        $pegawai = new Pegawai();
        $pegawai->name = $request->name;
        $pegawai->id_user = $user->id;
        $pegawai->umur = $request->umur;
        $pegawai->alamat = $request->alamat;
        $pegawai->id_bagian = $request->id_bagian;
        $pegawai->save();

        return redirect('/admin/akunpegawai/datapegawai');
    }

    public function deleteakunpegawai(Request $request, $id)
    {
        $pegawai = Pegawai::where('id', $id)->delete();

        return redirect('/admin/akunpegawai/datapegawai');
    }

    //Akun Atasan
    public function indexakunatasan()
    {
        $atasan = Atasan::get();
        return view('admin.akunatasan.index', compact('atasan'));
    }

    public function indexaddakunatasan()
    {
        return view('admin.akunatasan.add');
    }

    public function addakunatasan(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password = Hash::make($request->password);
        $user->save();

        $atasan = new Atasan();
        $atasan->name = $request->name;
        $atasan->id_user = $user->id;
        $atasan->umur = $request->umur;
        $atasan->alamat = $request->alamat;
        $atasan->save();

        return redirect('/admin/akunatasan/dataatasan');
    }

    public function deleteakunatasan(Request $request, $id)
    {
        $atasan = Atasan::where('id', $id)->delete();

        return redirect('/admin/akunatasan/dataatasan');
    }
}
