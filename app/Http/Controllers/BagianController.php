<?php

namespace App\Http\Controllers;

use App\Models\Bagian;
use Illuminate\Http\Request;

class BagianController extends Controller
{
    public function indexbagian()
    {
        $bagian = Bagian::get();
        return view('admin.bagian_pegawai.index', compact('bagian'));
    }

    public function indexaddbagian()
    {
        return view('admin.bagian_pegawai.add');
    }

    public function actionadd(Request $request)
    {
        $bagian = new Bagian();
        $bagian->name = $request->name;
        $bagian->save();
        return redirect('/admin/databagian');
    }

    public function deletebagian(Request $request, $id)
    {
        $bagian = Bagian::where('id', $id)->delete();

        return redirect('/admin/databagian');
    }
}
