<?php

namespace App\Http\Controllers;

use App\Models\Laporan_Pemusnahan;
use App\Models\Pengajuan_Pembuangan;
use Illuminate\Http\Request;

class Laporan_PemusnahanController extends Controller
{
    public function index()
    {
        $laporanPemusnahan = Laporan_Pemusnahan::get();

        return view('admin.laporan_pemusnahan.index', compact('laporanPemusnahan'));
    }

    public function indexadd()
    {
        $pemusnahan = Pengajuan_Pembuangan::where('apv_spv_k3', '>', 0)->where('apv_mnj_k3', '>', 0)->where('apv_admin', '>', 0)->get();
        // dd($pemusnahan);

        return view('admin.laporan_pemusnahan.add', compact('pemusnahan'));
    }

    public function indexaddbyid($id)
    {
        $pemusnahan = Pengajuan_Pembuangan::where('id',$id)->first();
        return view('admin.laporan_pemusnahan.addbyid', compact('pemusnahan'));
    }

    public function actionadd(Request $request)
    {
        $namafile = time() . '.' . $request->bukti->extension();
        $request->bukti->move(storage_path('buktipemusnahan'), $namafile);

        // dd($request->input());
        $pemusnahans = new Laporan_Pemusnahan();
        $pemusnahans->id_pemusnahan = $request->id;
        $pemusnahans->bukti = $namafile;
        $pemusnahans->save();

        return redirect('/admin/laporanpemusnahan');
    }

    public function downloadbukti($id)
    {
        $pemusnahans = Laporan_Pemusnahan::where('id',$id)->first();
        
        return response()->download(storage_path('buktipemusnahan/'.$pemusnahans->bukti));
    }
}
