<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function dashboardamin()
    {
        return view('admin.dashboard');
    }

    public function dashboardmanajer()
    {
        return view('manajer.dashboard');
    }

    public function dashboardsupervisor()
    {
        return view('supervisor.dashboard');
    }

    public function dashboardtim_k3()
    {
        return view('tim_k3.dashboard');
    }

    public function dashboarduser_bagian()
    {
        return view('user_bagian.dashboard');
    }
}
