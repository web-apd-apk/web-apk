<?php

namespace App\Http\Controllers;

use App\Models\Pengajuan_Pembuangan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Pengajuan_PembuanganController extends Controller
{
    public function index()
    {
        $pemusnahan = Pengajuan_Pembuangan::get();
        return view('admin.pemusnahan.index', compact('pemusnahan'));
    }

    public function indexadd()
    {
        
        return view('admin.pemusnahan.add');
    }

    public function actionadd(Request $request)
    {
        // dd($request->input());
        $pemusnahan = new Pengajuan_Pembuangan();
        $pemusnahan->pemusnah = $request->pemusnah;
        $pemusnahan->pemilik = $request->pemilik;
        $pemusnahan->id_apd = $request->apd;
        $pemusnahan->jumlah = $request->jumlah;
        $pemusnahan->save();

        return redirect('/admin/pengajuanpemusnahan');
    }

    public function approveadmin()
    {
        $pemusnahan = Pengajuan_Pembuangan::get();
        return view('admin.approveadmin.indexpemusnahan', compact('pemusnahan'));
    }

    public function approvedbyadmin($id)
    {
        $pemusnahan = Pengajuan_Pembuangan::where('id',$id)->first();
        $pemusnahan->apv_admin = Auth::user()->id;
        $pemusnahan->save();

        return redirect('/admin/approvepemusnahan');
    }

    public function declinedbyadmin($id)
    {
        $pemusnahan = Pengajuan_Pembuangan::where('id',$id)->first();
        $pemusnahan->apv_admin = -1;
        $pemusnahan->save();

        return redirect('/admin/approvepemusnahan');
    }

    public function approvemanajer()
    {
        $pemusnahan = Pengajuan_Pembuangan::get();
        return view('manajer.approvemanajer.indexpemusnahan', compact('pemusnahan'));
    }

    public function approvedbymanajer($id)
    {
        $pemusnahan = Pengajuan_Pembuangan::where('id',$id)->first();
        $pemusnahan->apv_mnj_k3 = Auth::user()->id;
        $pemusnahan->save();

        return redirect('/manajer/approvepemusnahan');
    }

    public function declinedbymanajer($id)
    {
        $pemusnahan = Pengajuan_Pembuangan::where('id',$id)->first();
        $pemusnahan->apv_mnj_k3 = -1;
        $pemusnahan->save();

        return redirect('/manajer/approvepemusnahan');
    }

    public function approvesupervisor()
    {
        $pemusnahan = Pengajuan_Pembuangan::get();
        return view('supervisor.approvesupervisor.indexpemusnahan', compact('pemusnahan'));
    }

    public function approvedbysupervisor($id)
    {
        $pemusnahan = Pengajuan_Pembuangan::where('id',$id)->first();
        $pemusnahan->apv_spv_k3 = Auth::user()->id;
        $pemusnahan->save();

        return redirect('/supervisor/approvepemusnahan');
    }

    public function declinedbysupervisor($id)
    {
        $pemusnahan = Pengajuan_Pembuangan::where('id',$id)->first();
        $pemusnahan->apv_spv_k3 = -1;
        $pemusnahan->save();

        return redirect('/supervisor/approvepemusnahan');
    }
}
