<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bagian extends Model
{
    protected $table = "bagian";

    protected $fillable = ["name"];

    use HasFactory;

    public function pegawai()
    {
        return $this->hasMany(Pegawai::class);
    }
}
