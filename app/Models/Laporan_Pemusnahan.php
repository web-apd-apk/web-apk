<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laporan_Pemusnahan extends Model
{
    protected $table = "laporan__pemusnahan";

    protected $fillable = ["id_pemusnahan", "bukti"];

    use HasFactory;

    public function pemusnahan()
    {
        return $this->belongsTo(Pengajuan_Pembuangan::class, 'id_pemusnahan');
    }
}
