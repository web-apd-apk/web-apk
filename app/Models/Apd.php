<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Apd extends Model
{
    protected $table = "apd";

    protected $fillable = ["name", "type", "safety_stock", "rop_stock"];

    use HasFactory;
}
