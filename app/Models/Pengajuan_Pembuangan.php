<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengajuan_Pembuangan extends Model
{
    protected $table = "pengajuan__pembuangan";

    protected $fillable = ["pemusnah", "pemilik", "id_apd", "jumlah", "apv_admin", "apv_spv_k3", "apv_mnj_k3"];

    use HasFactory;

    public function apd()
    {
        return $this->belongsTo(Apd::class, 'id_apd');
    }

    public function admin()
    {
        return $this->belongsTo(User::class, 'apv_admin');
    }

    public function spvk3()
    {
        return $this->belongsTo(User::class, 'apv_spv_k3');
    }

    public function mnjk3()
    {
        return $this->belongsTo(User::class, 'apv_mnj_k3');
    }
}
