<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
    protected $table = "laporan_kerusakan";

    protected $fillable = ["id_user", "id_inspeksi", "keterangan", "tindak_lanjut"];

    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function inspeksi()
    {
        return $this->belongsTo(Inspeksi::class, 'id_inspeksi');
    }
}
