<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inspeksi extends Model
{
    protected $table = "inspeksi";

    protected $fillable = ["id_user", "lokasi", "id_bagian", "id_apd", "jumlah", "kondisi", "tindak_lanjut", "keterangan"];

    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function bagian()
    {
        return $this->belongsTo(Bagian::class, 'id_bagian');
    }

    public function apd()
    {
        return $this->belongsTo(Apd::class, 'id_apd');
    }
}
