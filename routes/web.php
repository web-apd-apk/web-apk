<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware(['auth']);

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/getdata/apd/{id}', [App\Http\Controllers\ApdController::class, 'getdataapd'])->name('get-data-apd');
Route::get('/download/buktipengajuan/{id}', [App\Http\Controllers\PengajuanController::class, 'downloadbukti'])->name('get-data-apd');
Route::get('/download/buktipemusnahan/{id}', [App\Http\Controllers\Laporan_PemusnahanController::class, 'downloadbukti'])->name('get-data-pemusnahan');

//Role Admin
Route::group(['middleware' => 'role:admin'], function () {

    //Dashboard
    Route::get('/admin/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardamin'])->name('dashboard-admin');

    //Bagian Pegawai
    Route::get('/admin/databagian', [\App\Http\Controllers\BagianController::class, 'indexbagian'])->name('indexbagian-admin');
    Route::get('/admin/add/databagian', [\App\Http\Controllers\BagianController::class, 'indexaddbagian'])->name('indexaddbagian-admin');
    Route::post('/admin/add-databagian', [App\Http\Controllers\BagianController::class, 'actionadd'])->name('actionadd-admin');
    Route::get('/admin/databagian/delete{id}', [\App\Http\Controllers\BagianController::class, 'deletebagian'])->name('deletebagian-admin');

    //Akun Pegawai
    Route::get('/admin/akunpegawai/datapegawai', [\App\Http\Controllers\AccountController::class, 'indexakunpegawai'])->name('indexakunpegawai-admin');
    Route::get('/admin/akunpegawai/add-datapegawai', [\App\Http\Controllers\AccountController::class, 'indexaddakunpegawai'])->name('indexaddakunpegawai-admin');
    Route::post('/admin/akunpegawai-add', [App\Http\Controllers\AccountController::class, 'addakunpegawai'])->name('addakunpegawai-admin');
    Route::get('/admin/akunpegawai/delete{id}', [\App\Http\Controllers\AccountController::class, 'deleteakunpegawai'])->name('deleteakunpegawai-admin');

    //Akun Atasan
    Route::get('/admin/akunatasan/dataatasan', [\App\Http\Controllers\AccountController::class, 'indexakunatasan'])->name('indexakunatasan-admin');
    Route::get('/admin/akunatasan/add-dataatasan', [\App\Http\Controllers\AccountController::class, 'indexaddakunatasan'])->name('indexaddakunatasan-admin');
    Route::post('/admin/akunatasan-add', [App\Http\Controllers\AccountController::class, 'addakunatasan'])->name('addakunatasan-admin');
    Route::get('/admin/akunatasan/delete{id}', [\App\Http\Controllers\AccountController::class, 'deleteakunatasan'])->name('deleteakunatasan-admin');

    //APD
    Route::get('/admin/apd', [\App\Http\Controllers\ApdController::class, 'index'])->name('indexapd-admin');
    Route::get('/admin/apd/add', [\App\Http\Controllers\ApdController::class, 'indexadd'])->name('indexaddapd-admin');
    Route::post('/admin/apd/add', [\App\Http\Controllers\ApdController::class, 'actionadd'])->name('actionaddapd-admin');
    Route::post('/admin/apd/updatestok', [\App\Http\Controllers\ApdController::class, 'updatestok'])->name('updatestokapd-admin');

    //Inspeksi Apd
    Route::get('/admin/inspeksiapd', [\App\Http\Controllers\InspeksiController::class, 'index'])->name('indexinspeksi-admin');
    Route::get('/admin/inspeksiapd/add', [\App\Http\Controllers\InspeksiController::class, 'indexadd'])->name('indexaddinspeksi-admin');
    Route::post('/admin/inspeksiapd/add', [\App\Http\Controllers\InspeksiController::class, 'actionadd'])->name('actionaddinspeksi-admin');

    //Inspeksi Apd
    Route::get('/admin/laporankerusakan', [\App\Http\Controllers\LaporanController::class, 'index'])->name('indexilaporan-admin');
    Route::get('/admin/laporankerusakan/add', [\App\Http\Controllers\LaporanController::class, 'indexadd'])->name('indexadd-admin');
    Route::get('/admin/laporankerusakan/add/{id}', [\App\Http\Controllers\LaporanController::class, 'indexaddbyid'])->name('indexaddbyid-admin');
    Route::post('/admin/laporankerusakan/add/{id}', [\App\Http\Controllers\LaporanController::class, 'actionaddbyid'])->name('actionaddbyid-admin');

    //Pengajuan APD
    Route::get('/admin/pengajuanapd', [\App\Http\Controllers\PengajuanController::class, 'index'])->name('indexipengajuan-admin');
    Route::get('/admin/pengajuanapd/add', [\App\Http\Controllers\PengajuanController::class, 'indexadd'])->name('indexaddipengajuan-admin');
    Route::post('/admin/pengajuanapd/add', [\App\Http\Controllers\PengajuanController::class, 'actionadd'])->name('actionaddipengajuan-admin');

    //Approve admin
    Route::get('/admin/approvepengajuan', [\App\Http\Controllers\PengajuanController::class, 'approveadmin'])->name('indexipengajuan-admin');
    Route::get('/admin/approvepengajuan/approve/{id}', [\App\Http\Controllers\PengajuanController::class, 'approvedbyadmin'])->name('indexipengajuan-admin');
    Route::get('/admin/approvepengajuan/decline/{id}', [\App\Http\Controllers\PengajuanController::class, 'declinedbyadmin'])->name('indexipengajuan-admin');
    Route::get('/admin/approvepemusnahan', [\App\Http\Controllers\Pengajuan_PembuanganController::class, 'approveadmin'])->name('indexpemusnahan-admin');
    Route::get('/admin/approvepemusnahan/approve/{id}', [\App\Http\Controllers\Pengajuan_PembuanganController::class, 'approvedbyadmin'])->name('indexpemusnahan-admin');
    Route::get('/admin/approvepemusnahan/decline/{id}', [\App\Http\Controllers\Pengajuan_PembuanganController::class, 'declinedbyadmin'])->name('indexpemusnahan-admin');

    //Pengajuan Pemusnahan
    Route::get('/admin/pengajuanpemusnahan', [\App\Http\Controllers\Pengajuan_PembuanganController::class, 'index'])->name('indexpengajuan-admin');
    Route::get('/admin/pengajuanpemusnahan/add', [\App\Http\Controllers\Pengajuan_PembuanganController::class, 'indexadd'])->name('indexaddpengajuan-admin');
    Route::post('/admin/pengajuanpemusnahan/add', [\App\Http\Controllers\Pengajuan_PembuanganController::class, 'actionadd'])->name('actionaddpengajuan-admin');

    //Laporan Pemusnahan
    Route::get('/admin/laporanpemusnahan', [\App\Http\Controllers\Laporan_PemusnahanController::class, 'index'])->name('indexlaporan-admin');
    Route::get('/admin/laporanpemusnahan/add', [\App\Http\Controllers\Laporan_PemusnahanController::class, 'indexadd'])->name('indexaddlaporan-admin');
    Route::get('/admin/laporanpemusnahan/add/{id}', [\App\Http\Controllers\Laporan_PemusnahanController::class, 'indexaddbyid'])->name('indexaddbyid-admin');
    Route::post('/admin/laporanpemusnahan/add/{id}', [\App\Http\Controllers\Laporan_PemusnahanController::class, 'actionadd'])->name('actionaddlaporan-admin');
});

//Role Tim K3
Route::group(['middleware' => 'role:tim_k3'], function () {

    //Dashboard
    Route::get('/tim_k3/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardtim_k3'])->name('dashboard-tim_k3');
});

//Role User_Bagian
Route::group(['middleware' => 'role:user_bagian'], function () {

    //Dashboard
    Route::get('/user_bagian/dashboard', [App\Http\Controllers\HomeController::class, 'dashboarduser_bagian'])->name('dashboard-user_bagian');
});

//Role Supervisor
Route::group(['middleware' => 'role:supervisor'], function () {

    //Dashboard
    Route::get('/supervisor/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardsupervisor'])->name('dashboard-supervisor');

    //Approve Supervisor
    Route::get('/supervisor/approvepengajuan', [\App\Http\Controllers\PengajuanController::class, 'approvesupervisor'])->name('indexipengajuan-supervisor');
    Route::get('/supervisor/approvepengajuan/approve/{id}', [\App\Http\Controllers\PengajuanController::class, 'approvedbysupervisor'])->name('indexipengajuan-supervisor');
    Route::get('/supervisor/approvepengajuan/decline/{id}', [\App\Http\Controllers\PengajuanController::class, 'declinedbysupervisor'])->name('indexipengajuan-supervisor');
    Route::get('/supervisor/approvepemusnahan', [\App\Http\Controllers\Pengajuan_PembuanganController::class, 'approvesupervisor'])->name('indexipemusnahan-supervisor');
    Route::get('/supervisor/approvepemusnahan/approve/{id}', [\App\Http\Controllers\Pengajuan_PembuanganController::class, 'approvedbysupervisor'])->name('indexipemusnahan-supervisor');
    Route::get('/supervisor/approvepemusnahan/decline/{id}', [\App\Http\Controllers\Pengajuan_PembuanganController::class, 'declinedbysupervisor'])->name('indexipemusnahan-supervisor');
});

//Role Manajer
Route::group(['middleware' => 'role:manajer'], function () {

    //Dashboard
    Route::get('/manajer/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardmanajer'])->name('dashboard-manajer');

    //Approve manajer
    Route::get('/manajer/approvepengajuan', [\App\Http\Controllers\PengajuanController::class, 'approvemanajer'])->name('indexipengajuan-manajer');
    Route::get('/manajer/approvepengajuan/approve/{id}', [\App\Http\Controllers\PengajuanController::class, 'approvedbymanajer'])->name('indexipengajuan-manajer');
    Route::get('/manajer/approvepengajuan/decline/{id}', [\App\Http\Controllers\PengajuanController::class, 'declinedbymanajer'])->name('indexipengajuan-manajer');
    Route::get('/manajer/approvepemusnahan', [\App\Http\Controllers\Pengajuan_PembuanganController::class, 'approvemanajer'])->name('indexipemusnahan-manajer');
    Route::get('/manajer/approvepemusnahan/approve/{id}', [\App\Http\Controllers\Pengajuan_PembuanganController::class, 'approvedbymanajer'])->name('indexipemusnahan-manajer');
    Route::get('/manajer/approvepemusnahan/decline/{id}', [\App\Http\Controllers\Pengajuan_PembuanganController::class, 'declinedbymanajer'])->name('indexipemusnahan-manajer');

});
require __DIR__ . '/auth.php';
